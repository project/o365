# Microsoft 365 Connector module

## INTRODUCTION

The Microsoft 365 Connector module gives you a connector service that makes it
possible for developers to easily connect to the Microsoft Graph API.

### More information

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/o365
* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/o365
* Documentation can be found here: https://www.drupal.org/docs/contributed-modules/office-365-connector

## REQUIREMENTS

This module requires the following modules (downloaded via composer.json):

* OAuth2 Client module (https://www.drupal.org/project/oauth2_client)
* External Authentication module (https://www.drupal.org/project/externalauth)
* Microsoft Graph Library for PHP (https://github.com/microsoftgraph/msgraph-sdk-php)
* Mimey used for conversion of mime types (https://github.com/Xantios/mimey)
* Add the following settings in settings.php:
  ```php
  $settings['o365'] = [
      'api_settings' => [
      'client_id' => '',
      'client_secret' => '',
      'tenant_id' => '',
    ],
  ];
  ```
  (values can be retrieved from
  Azure, see https://www.drupal.org/docs/contributed-modules/office-365-connector/setting-up-an-azure-app
  for more info)

## MAINTAINERS

* Fabian de Rijk - https://www.drupal.org/u/fabianderijk

## Sponsors

### Finalist
Finalist implements Drupal CMS based on the needs of our customers: smart &
lightweight to highly complex & sophisticated websites, communities and
portals. Through our proven project approach and high involvement we are
able to produce websites within a stipulated time period. Regardless of
complexity or scale, we guarantee full service, a risk-free project
approach, transparent pricing models and vendor independency.
