<?php

/**
 * @file
 * Hook examples and explanations.
 */

/**
 * @addtogroup hooks
 */

/**
 * Create a list of authorization scopes.
 *
 * @param array $scopes
 *   An array of authorization scopes.
 */
function hook_o365_auth_scopes(array &$scopes) {
  $scopes[] = 'scope_name';
  $scopes[] = 'User.Read';
}
