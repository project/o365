<?php

namespace Drupal\o365;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Some constants we use in the code.
 */
class ConstantsService {

  /**
   * The config factory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The modules API config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $apiConfig;

  /**
   * The modules API settings like the Client ID.
   *
   * @var array
   */
  protected $apiSettings;

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The url where Microsoft will redirect us too.
   *
   * @var string
   */
  private $redirectUrl = '/o365/callback';

  /**
   * The authorize endpoint root.
   *
   * @var string
   */
  private $authorizeRoot = 'https://login.microsoftonline.com/';

  /**
   * The authorize endpoint path.
   *
   * @var string
   */
  private $authorizePath = '/oauth2/v2.0/authorize';

  /**
   * The token endpoint root.
   *
   * @var string
   */
  private $tokenRoot = 'https://login.microsoftonline.com/';

  /**
   * The token endpoint path.
   *
   * @var string
   */
  private $tokenPath = '/oauth2/v2.0/token';

  /**
   * The name of the temp store.
   *
   * @var string
   */
  private $userTempStoreName = 'o365.tempstore';

  /**
   * The name of the data saved in the temp store.
   *
   * @var string
   */
  private $userTempStoreDataName = 'o365AuthData';

  /**
   * Constructs a new ConstantsService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory interface.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack object.
   * @param \Drupal\o365\HelperService $helperService
   *   The helper service used to get the api settings.
   */
  public function __construct(ConfigFactoryInterface $configFactory, RequestStack $requestStack, HelperService $helperService) {
    $this->configFactory = $configFactory;
    $this->apiConfig = $this->configFactory->get('o365.api_settings');
    $this->apiSettings = $helperService->getApiConfig();
    $this->request = $requestStack->getCurrentRequest();
  }

  /**
   * Get the redirect URL.
   *
   * @return string
   *   The redirect url.
   */
  public function getRedirectUrl() {
    return 'https://' . $this->request->getHost() . $this->redirectUrl;
  }

  /**
   * Get the authorize url.
   *
   * @return string
   *   The authorize url.
   */
  public function getAuthorizeUrl() {
    $tenant = $this->getTenant();
    return $this->authorizeRoot . $tenant . $this->authorizePath;
  }

  /**
   * Get the token url.
   *
   * @return string
   *   The token url.
   */
  public function getTokenUrl() {
    $tenant = $this->getTenant();
    return $this->tokenRoot . $tenant . $this->tokenPath;
  }

  /**
   * Get the user temp store name.
   *
   * @return string
   *   The user temp store name.
   */
  public function getUserTempStoreName() {
    return $this->userTempStoreName;
  }

  /**
   * Get the user temp store data name.
   *
   * @return string
   *   The user temp store data name.
   */
  public function getUserTempStoreDataName() {
    return $this->userTempStoreDataName;
  }

  /**
   * Get the tenant ID.
   *
   * @return array|mixed|string|null
   *   The tenant ID.
   */
  private function getTenant() {
    return empty($this->apiSettings['tenant_id']) ? 'common' : $this->apiSettings['tenant_id'];
  }

}
