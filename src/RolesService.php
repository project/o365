<?php

declare(strict_types=1);

namespace Drupal\o365;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\user\UserInterface;

/**
 * Class that handles the adding / removing of roles of users.
 */
final class RolesService {

  /**
   * A list of safe roles that won't be deleted.
   *
   * @var string[]
   */
  private $safeRoles = [];

  /**
   * Constructs a RolesService object.
   */
  public function __construct(
    private readonly GraphService $graphService,
    private readonly ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * Add and remove roles that are defined.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function handleRoles(UserInterface $account) {
    $roles = $this->getConfiguredRoles();
    if (!empty($roles)) {
      $userRoles = [];
      $groups = $this->getGroupsFromOffice();
      if (!empty($groups)) {
        // Loop through the defined roles and check if they are in the groups.
        foreach ($roles as $officeValue => $drupalRoles) {
          // Loop through the groups.
          foreach ($groups as $group) {
            if (in_array($officeValue, $group)) {
              $userRoles = array_merge($drupalRoles, $userRoles);
            }
          }
        }
      }

      // Delete all existing roles without the locked ones.
      $existingRoles = $account->getRoles(TRUE);
      if (!empty($existingRoles)) {
        foreach ($existingRoles as $role) {
          if (!empty($this->safeRoles) && !in_array($role, $this->safeRoles)) {
            $account->removeRole($role);
          }
        }
      }

      // Add the new roles.
      if (!empty($userRoles)) {
        foreach ($userRoles as $role) {
          $account->addRole($role);
        }
      }

      // Save the account.
      $account->save();
    }
  }

  /**
   * Get the configured roles from config.
   *
   * @return array
   *   The list of groups and roles.
   */
  private function getConfiguredRoles(): array {
    $roleConfig = $this->configFactory->get('o365.role_settings');
    $rolesMap = $roleConfig->get('roles_map');
    $roles = [];
    if (!empty($rolesMap)) {
      $mapping = trim($roleConfig->get('roles_map'));
      $mapping = explode("\n", $mapping);
      $mapping = array_map('trim', $mapping);

      foreach ($mapping as $text) {
        $exploded = explode('|', $text);
        $key = array_shift($exploded);
        $roles[$key] = $exploded;
      }
    }

    // Load the safe roles.
    $safeRoles = $roleConfig->get('safe_roles');
    if ($safeRoles !== NULL && !empty($safeRoles)) {
      $safeRoles = explode("\n", $safeRoles);
      $this->safeRoles = array_map('trim', $safeRoles);
    }

    return $roles;
  }

  /**
   * Get the user groups from Microsoft 365.
   *
   * @return array
   *   The groups.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  private function getGroupsFromOffice(): array {
    $groups = $this->graphService->getGraphData('/me/transitiveMemberOf/microsoft.graph.group?$select=id,displayName,onPremisesSamAccountName');
    return $groups['value'];
  }

}
