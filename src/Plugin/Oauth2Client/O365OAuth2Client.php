<?php

namespace Drupal\o365\Plugin\Oauth2Client;

use Drupal\oauth2_client\Plugin\Oauth2Client\Oauth2ClientPluginBase;
use League\OAuth2\Client\Token\AccessTokenInterface;

/**
 * OAuth2 Client to authenticate with Microsoft 365.
 *
 * @Oauth2Client(
 *   id = "o365",
 *   name = @Translation("Microsoft 365"),
 *   grant_type = "client_credentials",
 *   client_id = "",
 *   client_secret = "",
 *   authorization_uri =
 *   "https://login.microsoftonline.com/common/oauth2/v2.0/authorize",
 *   token_uri = "https://login.microsoftonline.com/common/oauth2/v2.0/token",
 *   resource_owner_uri = "",
 *   credential_provider = "o365_sso",
 *   storage_key = "",
 * )
 */
class O365OAuth2Client extends Oauth2ClientPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getClientId(): string {
    $config = $this->configFactory->get('o365.api_settings');
    return $config->get('client_id');
  }

  /**
   * {@inheritdoc}
   */
  public function getClientSecret(): string {
    $config = $this->configFactory->get('o365.api_settings');
    return $config->get('client_secret');
  }

  /**
   * {@inheritdoc}
   */
  public function storeAccessToken(AccessTokenInterface $accessToken): void {
    $this->state->set(
      'oauth2_client_access_token-' . $this->getId(),
      $accessToken
    );
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveAccessToken(): AccessTokenInterface|NULL {
    return $this->state->get('oauth2_client_access_token-' . $this->getId());
  }

  /**
   * {@inheritdoc}
   */
  public function clearAccessToken(): void {
    $this->state->delete('oauth2_client_access_token-' . $this->getId());
  }

  /**
   * {@inheritdoc}
   */
  public function getCredentialProvider(): ?string {
    return 'o365_sso';
  }

}
