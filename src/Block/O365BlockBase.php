<?php

namespace Drupal\o365\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\o365\GraphService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Custom block base for o365 for access checking.
 *
 * @phpstan-consistent-constructor
 */
class O365BlockBase extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Construct a O365BlockBase.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\o365\GraphService $graphService
   *   The o365 Graph service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected GraphService $graphService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('o365.graph')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    $access = AccessResult::forbidden();
    // Anonymous can never view these blocks.
    if (!$account->isAnonymous()) {
      // Allow access when user is a Microsoft user.
      if ($this->graphService->getCurrentUserId()) {
        $access = AccessResult::allowed();
      }
    }

    return $return_as_object ? $access : $access->isAllowed();
  }

}
