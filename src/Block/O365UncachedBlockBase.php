<?php

namespace Drupal\o365\Block;

use Drupal\Core\Cache\UncacheableDependencyTrait;

/**
 * Extending the O365BlockBase with a non-cachable dependency.
 */
class O365UncachedBlockBase extends O365BlockBase {

  use UncacheableDependencyTrait;

}
