<?php

namespace Drupal\o365;

/**
 * The interface for the AuthenticationService.
 */
interface AuthenticationServiceInterface {

  /**
   * Redirect the user to the correct Microsoft pages for oAuth2.
   */
  public function redirectToAuthorizationUrl();

  /**
   * Generate and save the accessToken.
   *
   * @param string $code
   *   The code we got from Microsoft.
   * @param mixed $redirect
   *   Either FALSE or a url where to redirect to.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */

  /**
   * Generate and save the accessToken.
   *
   * @param string $code
   *   The code we got from Microsoft.
   * @param bool|string $redirect
   *   Either FALSE or an url where to redirect to.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  public function setAccessToken($code, $redirect);

  /**
   * Get the access token for the user.
   *
   * @param bool $login
   *   If we want to log in the user if there is no token.
   *
   * @return string|false
   *   The access token or false when no token is provided.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  public function getAccessToken($login = FALSE);

  /**
   * Save the auth data from the url parameters in the user session storage.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function saveAuthDataFromUrl();

  /**
   * Check if a user is logged in via Office.
   *
   * @return string|bool
   *   The users ID or FALSE if not logged in via Office.
   */
  public function checkForOfficeLogin();

}
