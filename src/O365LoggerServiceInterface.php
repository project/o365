<?php

namespace Drupal\o365;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * The interface for the O365LoggerService service.
 */
interface O365LoggerServiceInterface {

  /**
   * Log a message into the watchdog.
   *
   * @param string $message
   *   The message.
   * @param string $severity
   *   The severity of the log message.
   */
  public function log($message, $severity);

  /**
   * Log a debug message into the watchdog.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $message
   *   The translatable message.
   */
  public function debug(TranslatableMarkup $message);

}
