<?php

namespace Drupal\o365\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form, used to set the API settings like client_id.
 */
class ApiSettingsForm extends ConfigFormBase {

  /**
   * The o365 helper service.
   *
   * @var \Drupal\o365\HelperService
   */
  protected $helperService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->helperService = $container->get('o365.helpers');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'o365.api_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('o365.api_settings');
    $form['redirect_login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect after login URL'),
      '#description' => $this->t('After login users will be redirected to this page. This needs to be a full url like https://www.example.com.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('redirect_login'),
      '#required' => TRUE,
    ];
    $form['redirect_callback'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Callback url'),
      '#description' => $this->t('This value should be: @url', [
        '@url' => 'https://' . $this->getRequest()->getHost() . '/o365/callback',
      ]),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => empty($config->get('redirect_callback')) ? 'https://' . $this->getRequest()->getHost() . '/o365/callback' : $config->get('redirect_callback'),
    ];

    $url = Url::fromRoute('o365.auth_scopes');
    $link = Link::fromTextAndUrl('here', $url);
    $form['auth_scopes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Authorization scopes'),
      '#description' => $this->t('A space separated list of authorization scopes. You can add some extra scopes, or use the o365_auth_scopes hook. You can view a list of needed scopes @link', ['@link' => $link->toString()]),
      '#default_value' => $config->get('auth_scopes'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('o365.api_settings')
      ->set('redirect_login', $form_state->getValue('redirect_login'))
      ->set('redirect_callback', $form_state->getValue('redirect_callback'))
      ->set('auth_scopes', $form_state->getValue('auth_scopes'))->save();
  }

}
