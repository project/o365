<?php

namespace Drupal\o365;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\externalauth\Authmap;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Service used to authenticate users between Microsoft 365 and Drupal.
 */
class AuthenticationService implements AuthenticationServiceInterface {

  use RedirectDestinationTrait;

  /**
   * The config factory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The modules API config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $apiConfig;

  /**
   * The modules API settings like the client ID.
   *
   * @var array
   */
  protected $apiSettings;

  /**
   * The modules base config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $moduleConfig;

  /**
   * The private temp store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * A oauth provider.
   *
   * @var \League\OAuth2\Client\Provider\GenericProvider
   */
  protected $oauthClient;

  /**
   * The ConstantsService implementation.
   *
   * @var \Drupal\o365\ConstantsService
   */
  protected $constants;

  /**
   * The logger service.
   *
   * @var \Drupal\o365\O365LoggerServiceInterface
   */
  protected $loggerService;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * If we want to add debug messages.
   *
   * @var bool
   */
  private $debug;

  /**
   * The auth data.
   *
   * @var array
   */
  private $authValues = [];

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The auth map.
   *
   * @var \Drupal\externalauth\Authmap
   */
  protected $authmap;

  /**
   * The o365 helper service.
   *
   * @var \Drupal\o365\HelperService
   */
  protected $helperService;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Constructs a new AuthenticationService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory interface.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempStoreFactory
   *   The private store factory.
   * @param \Drupal\o365\ConstantsService $constantsService
   *   The constants service from the o365 module.
   * @param \Drupal\o365\O365LoggerServiceInterface $loggerService
   *   The logger service from the o365 module.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\o365\HelperService $helperService
   *   The helper service used to get the api settings.
   * @param \Drupal\Core\Session\AccountProxyInterface $accountProxy
   *   The account proxy for the current user.
   * @param \Drupal\externalauth\Authmap $authmap
   *   The auth map.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger class.
   */
  public function __construct(ConfigFactoryInterface $configFactory, PrivateTempStoreFactory $tempStoreFactory, ConstantsService $constantsService, O365LoggerServiceInterface $loggerService, RequestStack $requestStack, HelperService $helperService, AccountProxyInterface $accountProxy, Authmap $authmap, Messenger $messenger) {
    $this->configFactory = $configFactory;
    $this->apiConfig = $this->configFactory->get('o365.api_settings');
    $this->apiSettings = $helperService->getApiConfig();
    $this->helperService = $helperService;
    $this->moduleConfig = $this->configFactory->get('o365.settings');
    $this->tempStore = $tempStoreFactory;
    $this->constants = $constantsService;
    $this->loggerService = $loggerService;
    $this->request = $requestStack->getCurrentRequest();
    $this->currentUser = $accountProxy;
    $this->authmap = $authmap;
    $this->messenger = $messenger;

    $this->debug = !empty($this->moduleConfig->get('verbose_logging'));
  }

  /**
   * {@inheritdoc}
   */
  public function redirectToAuthorizationUrl() {
    if ($this->currentUser->isAnonymous() && strstr($this->request->getPathInfo(), '/o365/login') === FALSE) {
      $message = t('An anonymous user tried to log in using o365, this is not allowed.');
      $this->loggerService->debug($message);

      $url = Url::fromRoute('user.login')->toString(TRUE);
      $response = new TrustedRedirectResponse($url->getGeneratedUrl());
      $response->addCacheableDependency($url);
      return $response;
    }

    if ($this->debug) {
      $message = t('-- redirectToAuthorizationUrl()');
      $this->loggerService->debug($message);
    }

    // Save the destination.
    $this->saveDestination();

    $clientId = $this->apiSettings['client_id'];

    if ($this->debug) {
      $message = t('clientId: @client', ['@client' => $clientId]);
      $this->loggerService->debug($message);
    }

    $this->generateProvider();

    $authUrl = $this->oauthClient->getAuthorizationUrl() . '&client_id=' . $clientId;
    $authMapUser = $this->authmap->get($this->currentUser->getAccount()->id(), 'o365_sso');

    if ($this->currentUser->isAnonymous() || $authMapUser) {
      return new TrustedRedirectResponse($authUrl);
    }

    throw new AccessDeniedHttpException('User is not authorized to access this page.');
  }

  /**
   * {@inheritdoc}
   */
  public function setAccessToken($code, $redirect = FALSE) {
    try {
      // Make the token request.
      $this->generateProvider();
      $accessToken = $this->oauthClient->getAccessToken('authorization_code', [
        'code' => $code,
        'client_id' => $this->apiSettings['client_id'],
        'client_secret' => $this->apiSettings['client_secret'],
      ]);

      $this->saveAuthData($accessToken);

      if ($redirect) {
        $redirect .= '?access_token=' . $accessToken->getToken();
        $redirect .= '&refresh_token=' . $accessToken->getRefreshToken();
        $redirect .= '&expires_on=' . $accessToken->getExpires();

        return new TrustedRedirectResponse($redirect);
      }
    }
    catch (IdentityProviderException $e) {
      $error = $e->getResponseBody();
      $message = t('Error description: @error', ['@error' => $error['error_description']]);
      $this->loggerService->log($message, 'error');
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken($login = TRUE) {
    $authData = $this->getAuthData();

    if (!empty($authData)) {
      $now = time();

      if ($authData['expires_on'] > $now) {
        return $authData['access_token'];
      }

      if ($this->debug) {
        $message = t('accessToken is expired, refresh the token');
        $this->loggerService->debug($message);
      }

      return $this->refreshToken($authData['refresh_token']);
    }

    // We don't have any auth data, so return a redirect response to log in
    // and back.
    if ($login) {
      return $this->redirectToAuthorizationUrl();
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function saveAuthDataFromUrl() {
    $this->authValues = [
      'access_token' => $this->request->get('access_token'),
      'refresh_token' => $this->request->get('refresh_token'),
      'expires_on' => $this->request->get('expires_on'),
    ];

    $this->saveDataToTempStore($this->constants->getUserTempStoreDataName(), $this->authValues);
  }

  /**
   * {@inheritdoc}
   */
  public function checkForOfficeLogin() {
    if (!empty($this->getAuthData())) {
      return $this->authmap->get($this->currentUser->id(), 'o365_sso');
    }

    return FALSE;
  }

  /**
   * Generate a new access token based on the refresh token.
   *
   * @param string $refreshToken
   *   The refresh token.
   *
   * @return string
   *   The new access token.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  private function refreshToken($refreshToken) {
    $this->generateProvider();

    $accessToken = $this->oauthClient->getAccessToken('refresh_token', [
      'refresh_token' => $refreshToken,
      'client_id' => $this->apiSettings['client_id'],
      'client_secret' => $this->apiSettings['client_secret'],
    ]);
    $this->saveAuthData($accessToken);
    return $accessToken->getToken();
  }

  /**
   * Generate a basic oAuth2 provider.
   */
  private function generateProvider() {
    $redirectUri = empty($this->apiConfig->get('redirect_callback')) ? 'https://' . $this->request->getHost() . '/o365/callback' : $this->apiConfig->get('redirect_callback');
    $scopes = $this->helperService->getAuthScopes();

    $providerData = [
      'client_id' => $this->apiSettings['client_id'],
      'client_secret' => $this->apiSettings['client_secret'],
      'redirectUri' => $redirectUri,
      'urlAuthorize' => $this->constants->getAuthorizeUrl(),
      'urlAccessToken' => $this->constants->getTokenUrl(),
      'urlResourceOwnerDetails' => '',
      'scopes' => $scopes,
    ];

    if ($this->debug) {
      $message = t('providerData: @data', ['@data' => print_r($providerData, TRUE)]);
      $this->loggerService->debug($message);
    }

    $this->oauthClient = new GenericProvider($providerData);
  }

  /**
   * Get the auth data from temp store or cookie.
   *
   * @return array
   *   The saved auth data.
   */
  private function getAuthData() {
    // Check if there is some auth data on the url, if so, save it.
    if (!empty($this->request->get('access_token'))) {
      $this->saveAuthDataFromUrl();
    }

    $tempstore = $this->tempStore->get($this->constants->getUserTempStoreName());
    return $tempstore->get($this->constants->getUserTempStoreDataName());
  }

  /**
   * Save the auth data to the temp store.
   *
   * @param \League\OAuth2\Client\Token\AccessTokenInterface $accessToken
   *   The access token object.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  private function saveAuthData(AccessTokenInterface $accessToken) {
    $this->authValues = [
      'access_token' => $accessToken->getToken(),
      'refresh_token' => $accessToken->getRefreshToken(),
      'expires_on' => $accessToken->getExpires(),
    ];

    if ($this->debug) {
      $message = t('Saving authData: @data', ['@data' => print_r($this->authValues, TRUE)]);
      $this->loggerService->debug($message);
    }

    $this->saveDataToTempStore($this->constants->getUserTempStoreDataName(), $this->authValues);
  }

  /**
   * Save data to the tempstore.
   *
   * @param string $name
   *   The name of the store.
   * @param mixed $value
   *   The value.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  private function saveDataToTempStore($name, $value) {
    $tempstore = $this->tempStore->get($this->constants->getUserTempStoreName());

    $tempstore->set($name, $value);
  }

  /**
   * Get data from the temp store.
   *
   * @param string $name
   *   The name of the temp store value.
   *
   * @return mixed
   *   The value.
   */
  public function getDataFromTempStore($name) {
    $tempstore = $this->tempStore->get($this->constants->getUserTempStoreName());
    return $tempstore->get($name);
  }

  /**
   * Save the destination that is part of the url.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  private function saveDestination() {
    $destination = NULL;
    if ($this->request->query->has('destination')) {
      $destination = $this->getRedirectDestination()->get();
      // Remove the destination to stop the RedirectResponseSubscriber from
      // picking it up.
      $this->request->query->remove('destination');
    }
    $this->saveDataToTempStore('destination', $destination);
  }

}
