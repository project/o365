<?php

namespace Drupal\o365;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Site\Settings;

/**
 * Service with some helpful methods that we use in different locations.
 */
class HelperService {

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new HelperService object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The DateFormatterInterface definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(DateFormatterInterface $date_formatter, ConfigFactoryInterface $configFactory, ModuleHandlerInterface $moduleHandler) {
    $this->dateFormatter = $date_formatter;
    $this->configFactory = $configFactory;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Create a ISO8601 timestamp that Microsoft Graph API can use.
   *
   * @param int $timestamp
   *   The unix timestamp we want to use to create the date.
   *
   * @return string
   *   The ISO8601 formatted date.
   */
  public function createIsoDate($timestamp) {
    return $this->dateFormatter->format($timestamp, 'custom', 'Y-m-d\TH:i:s.000') . 'Z';
  }

  /**
   * Format a ISO8601 date into something more readable.
   *
   * @param string $date
   *   The ISO8601 date.
   * @param string $timezone
   *   The timezone.
   * @param string $format
   *   The format we want to convert to.
   *
   * @return string
   *   The formatted date.
   *
   * @throws \Exception
   */
  public function formatDate($date, $timezone = 'UTC', $format = 'd-m-Y H:i') {
    $dateTimezone = new \DateTimeZone($timezone);
    $dateTime = new \DateTime($date, $dateTimezone);
    $timestamp = (int) $dateTime->format('U');

    return $this->dateFormatter->format($timestamp, 'custom', $format);
  }

  /**
   * Retrieve the API settings from the config.
   *
   * @return false|array
   *   The array with the settings, or FALSE.
   */
  public function getApiConfig() {
    $settings = Settings::get('o365');
    return $settings['api_settings'] ?? FALSE;
  }

  /**
   * Get the auth scopes string.
   *
   * @param bool $asArray
   *   If the returned value needs to be a array, defaults to FALSE.
   *
   * @return string|array
   *   The space seperated list of auth scopes or a raw array of scopes.
   */
  public function getAuthScopes($asArray = FALSE) {
    // Get the scopes from the config.
    $config = $this->configFactory->get('o365.api_settings');
    $authScopes = $config->get('auth_scopes') ?? '';
    $scopes = explode(' ', $authScopes);

    // Get the scopes from the hook implementations.
    $this->moduleHandler->invokeAll('o365_auth_scopes', [&$scopes]);

    // Remove duplicate and empty values.
    $scopes = array_unique($scopes);
    $scopes = array_filter($scopes);

    // We need to check for the offline_access value.
    if (!in_array('offline_access', $scopes)) {
      $scopes[] = 'offline_access';
    }

    // Return as array.
    if ($asArray) {
      return $scopes;
    }

    // Return a explodeed string.
    return implode(' ', $scopes);
  }

  /**
   * Generate a unix timestamp from a date.
   *
   * @param string $date
   *   The date.
   * @param string $timezone
   *   The timezone.
   *
   * @return string
   *   The unix timestamp.
   */
  public function getTsFromDate($date, $timezone = 'UTC') {
    $dateTimezone = new \DateTimeZone($timezone);
    $dateTime = new \DateTime($date, $dateTimezone);

    return $dateTime->format('U');
  }

}
