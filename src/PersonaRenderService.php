<?php

namespace Drupal\o365;

/**
 * Service that renders persona information based on the type.
 *
 * @package Drupal\o365
 */
class PersonaRenderService {

  /**
   * Render the persona based on a type (small, medium, large)
   *
   * @param array $userData
   *   The profile array as retrieved from the Graph API.
   * @param string $type
   *   The way we want to render the array.
   *
   * @return array
   *   The render array for the profile.
   */
  public function renderPersona(array $userData, $type = 'small') {
    $return = [
      '#theme' => 'o365_persona_render_' . $type,
      '#userData' => $userData,
      '#attached' => [
        'library' => [
          'o365/persona',
        ],
      ],
    ];

    return $return;
  }

  /**
   * Return a (random) color.
   *
   * @return string
   *   The random color.
   *
   * @see https://developer.microsoft.com/en-us/fluentui#/styles/web/colors/personas
   */
  public function getRandomPersonaColor() {
    $colors = [
      'PinkRed10' => '#750B1C',
      'Red20' => '#a4262c',
      'Red10' => '#d13438',
      'Orange20' => '#ca5010',
      'OrangeYellow20' => '#986f0b',
      'Green10' => '#498205',
      'Green20' => '#0b6a0b',
      'Cyan20' => '#038387',
      'Cyan30' => '#005b70',
      'CyanBlue10' => '#0078d4',
      'CyanBlue20' => '#004e8c',
      'Blue10' => '#4f6bed',
      'BlueMagenta30' => '#5c2e91',
      'BlueMagenta20' => '#8764b8',
      'Magenta20' => '#881798',
      'Magenta10' => '#c239b3',
      'MagentaPink10' => '#e3008c',
      'Orange30' => '#8e562e',
      'Gray30' => '#7a7574',
      'Gray20' => '#69797e',
    ];

    return array_rand($colors);
  }

}
