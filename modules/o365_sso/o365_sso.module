<?php

/**
 * @file
 * Contains o365_sso.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function o365_sso_form_user_login_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  o365_sso_alter_login_form($form, $form_state, $form_id);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function o365_sso_user_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id === 'user_form') {
    $config = \Drupal::config('o365_sso.settings');
    // We want to hide email, password and email fields for o365 users.
    if (!empty($config->get('hide_user_fields'))) {
      /** @var \Drupal\user\Entity\User $user */
      $user = \Drupal::routeMatch()->getParameter('user');
      /** @var \Drupal\externalauth\Authmap $authMap */
      $authMap = \Drupal::service('externalauth.authmap');
      $authMapUser = $authMap->get($user->id(), 'o365_sso');
      // Only do this for users that are a o365_sso authmap user.
      if ($authMapUser) {
        $form['account']['mail']['#access'] = FALSE;
        $form['account']['name']['#access'] = FALSE;
        $form['account']['pass']['#access'] = FALSE;
        $form['account']['current_pass']['#access'] = FALSE;
      }
    }
  }
}

/**
 * Function that we can use to change the login form.
 *
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state object.
 * @param string $form_id
 *   The form id.
 */
function o365_sso_alter_login_form(array &$form, FormStateInterface $form_state, $form_id) {
  $config = \Drupal::config('o365_sso.settings');
  $request = \Drupal::request();
  $form['#attached']['library'][] = 'o365/general';
  $form['#attached']['library'][] = 'o365/icons';

  // Disable cache for this form. This makes the redirect work if enabled.
  $form['#cache'] = [
    'max-age' => 0,
  ];

  // Hide form fields when this option is enabled.
  $hideForm = $config->get('hide_login_form');
  if (!empty($hideForm)) {
    $showLogin = \Drupal::request()->query->get('show_login');
    // Only hide when the show_login parameter is not true.
    if ($showLogin !== 'true') {
      $form['name']['#access'] = FALSE;
      $form['pass']['#access'] = FALSE;
      $form['more-links']['#access'] = FALSE;
      $form['actions']['submit']['#access'] = FALSE;
    }
  }

  $redirectDestination = $config->get('redirect_login_destination');
  $query = [];
  if (!empty($redirectDestination)) {
    $destination = \Drupal::request()->query->get('destination');
    if (!$destination) {
      $destination = \Drupal::request()->query->get('prev_path');
    }
    $query = ['destination' => $destination];
  }

  // Only redirect when auto redirect is enabled and the path does not contain
  // the local login link.
  if (!empty($config->get('auto_redirect')) && strstr($request->getPathInfo(), '/user/drupal_login') === FALSE) {
    $url = Url::fromRoute('o365_sso.login_controller_login', [], [
      'absolute' => TRUE,
      'query' => $query,
    ]);
    $response = new RedirectResponse($url->toString());
    $response->send();
  }

  $linkWeight = -99;
  if (empty($config->get('link_position'))) {
    $linkWeight = 99;
  }

  $form['o365_actions'] = [
    '#type' => 'actions',
    '#weight' => $form['actions']['#weight'] ?? $linkWeight,
  ];

  $configuredText = $config->get('link_text') ?? 'Login with Microsoft 365';
  $linkText = Markup::create('<span class="ms-Icon ms-Icon--OfficeLogo o365--mr-12"></span>' . $configuredText);

  $form['o365_actions']['sso_login_link'] = [
    '#title' => $linkText,
    '#type' => 'link',
    '#url' => Url::fromRoute('o365_sso.login_controller_login', [], ['query' => $query]),
    '#attributes' => [
      'class' => ['button--primary button-o365-sso button'],
    ],
  ];
}
