<?php

namespace Drupal\o365_sso\Controller;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\o365\AuthenticationService;
use Drupal\o365\GraphService;
use Drupal\o365\O365LoggerServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * LoginCallbackController. Call when returning from the o365 service.
 */
class LoginCallbackController extends ControllerBase {

  /**
   * The authentication service, used to handle all kinds of auth stuff.
   *
   * @var \Drupal\o365\AuthenticationService
   */
  protected $authenticationService;

  /**
   * The o365 GraphService.
   *
   * @var \Drupal\o365\GraphService
   */
  protected $graphService;

  /**
   * The o365 logger service.
   *
   * @var \Drupal\o365\O365LoggerServiceInterface
   */
  protected $loggerService;

  /**
   * Constructs a new LoginController object.
   *
   * @param \Drupal\o365\AuthenticationService $authenticationService
   *   The AuthenticationService definition.
   * @param \Drupal\o365\GraphService $graphService
   *   The GraphService definition.
   * @param \Drupal\o365\O365LoggerServiceInterface $loggerService
   *   The custom logger service for the o365 module.
   */
  public function __construct(AuthenticationService $authenticationService, GraphService $graphService, O365LoggerServiceInterface $loggerService) {
    $this->authenticationService = $authenticationService;
    $this->graphService = $graphService;
    $this->loggerService = $loggerService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('o365.authentication'), $container->get('o365.graph'), $container->get('o365.logger'));
  }

  /**
   * Callback for the login.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return mixed
   *   A redirect to the set URL.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  public function callback(Request $request) {
    $authCode = $request->get('code');
    $error = $request->get('error');

    if ($authCode) {
      $redirectUrl = Url::fromRoute('o365_sso.user_login_controller_login')->toString(TRUE);
      $response = $this->authenticationService->setAccessToken($authCode, $redirectUrl->getGeneratedUrl());
      if ($response) {
        if ($response instanceof CacheableResponseInterface) {
          $response->addCacheableDependency($redirectUrl);
        }
        return $response;
      }
    }

    // If there is an error, show it on the page.
    if ($error) {
      $errorDescription = $request->get('error_description');
      $message = $error;

      if ($errorDescription) {
        $message = $errorDescription;
      }

      $this->loggerService->log($message, 'error');
      $this->messenger()->addError($message);
    }

    return [
      '#type' => 'markup',
      '#markup' => $this->t('The authorisation code has not been provided. Please try again.'),
    ];
  }

}
