<?php

namespace Drupal\o365_sso;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;

/**
 * Access checker for the login page.
 */
class O365CheckLoginAccess {

  /**
   * Check if a user is anonymous and grant access, else deny.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   If a user has access.
   */
  public function checkAccess(): AccessResultForbidden|AccessResultAllowed {
    $currentUser = \Drupal::currentUser();

    return $currentUser->isAnonymous() ? AccessResult::allowed() : AccessResult::forbidden('This path is only accessible for anonymous users');
  }

}
