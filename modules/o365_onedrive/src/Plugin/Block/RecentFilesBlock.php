<?php

namespace Drupal\o365_onedrive\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\o365\Block\O365UncachedBlockBase;
use Drupal\o365\GraphService;
use Drupal\o365_onedrive\GetFilesAndFoldersServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Recent Files' block.
 *
 * @Block(
 *   id = "o365_recent_files",
 *   admin_label = @Translation("Recent Files"),
 *   category = @Translation("Microsoft 365")
 * )
 */
final class RecentFilesBlock extends O365UncachedBlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\o365_onedrive\GetFilesAndFoldersServiceInterface definition.
   *
   * @var \Drupal\o365_onedrive\GetFilesAndFoldersServiceInterface
   */
  protected GetFilesAndFoldersServiceInterface $getFilesAndFoldersService;

  /**
   * Constructs a new RecentFilesBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\o365\GraphService $graphService
   *   The o365 Graph service.
   * @param \Drupal\o365_onedrive\GetFilesAndFoldersServiceInterface $getFilesAndFoldersService
   *   The GetFilesAndFoldersServiceInterface definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GraphService $graphService, GetFilesAndFoldersServiceInterface $getFilesAndFoldersService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $graphService);
    $this->getFilesAndFoldersService = $getFilesAndFoldersService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('o365.graph'),
      $container->get('o365_onedrive.get_files')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   * @throws \Microsoft\Graph\Exception\GraphException
   */
  public function build(): array {
    $config = $this->getConfiguration();
    $numFiles = ($config['num_shared_files']) ?? 5;
    $blockData = $this->getFilesAndFoldersService->listSpecialFilesAndFolders('recent', $numFiles);

    if (!empty($blockData)) {
      return $blockData;
    }

    if ($config['show_recent_files_block']) {
      return [
        '#theme' => 'o365_onedrive_results',
        '#results' => $config['recent_files_block_text'],
      ];
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['show_recent_files_block'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show recent files when there is no data'),
      '#default_value' => ($config['show_recent_files_block']) ?? FALSE,
    ];

    $form['num_shared_files'] = [
      '#type' => 'number',
      '#title' => $this->t('The number of files to show'),
      '#default_value' => ($config['num_shared_files']) ?? 5,
    ];

    $form['recent_files_block_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Recent files block text'),
      '#description' => $this->t('Enter the text you would like to show'),
      '#placeholder' => $this->t('Enter the text you would like to show'),
      '#default_value' => $config['recent_files_block_text'] ?? $this->t('No recent files found.'),
      '#states' => [
        'visible' => [
          ':input[name="settings[show_recent_files_block]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $this->configuration['show_recent_files_block'] = $values['show_recent_files_block'];
    $this->configuration['recent_files_block_text'] = $values['recent_files_block_text'];
    $this->configuration['num_shared_files'] = $values['num_shared_files'];
  }

}
