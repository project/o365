<?php

namespace Drupal\o365_onedrive;

use Drupal\o365\GraphService;
use Mimey\MimeTypes;

/**
 * Class used to retrieve folder listings from onedrive.
 */
class GetFilesAndFoldersService implements GetFilesAndFoldersServiceInterface {

  /**
   * Drupal\o365\GraphService definition.
   *
   * @var \Drupal\o365\GraphService
   */
  protected GraphService $graphService;

  /**
   * The drive array with all the values.
   *
   * @var array|bool
   */
  protected array|bool $drive;

  /**
   * Constructs a new GetFilesAndFoldersService object.
   *
   * @param \Drupal\o365\GraphService $o365_graph
   *   The GraphService definition.
   */
  public function __construct(GraphService $o365_graph) {
    $this->graphService = $o365_graph;
  }

  /**
   * {@inheritdoc}
   */
  public function listFilesAndFolders($folder = FALSE) {
    $endPoint = '/me/drive/root/children?$top=5';
    if ($folder) {
      $endPoint = '/me/drive/items/' . $folder . '/children?$top=10';
    }

    $this->getDrive($endPoint);
    return $this->renderFileList();
  }

  /**
   * {@inheritdoc}
   */
  public function listSharedFilesAndFolders(int $limit = 10) {
    $endPoint = '/me/drive/sharedWithMe?$top=' . $limit;

    $this->getDrive($endPoint);
    return $this->renderFileList($limit);
  }

  /**
   * {@inheritdoc}
   */
  public function listSpecialFilesAndFolders(string $type, int $limit = 5) {
    $endPoint = '/me/drive/' . $type . '?$top=' . $limit;

    $this->getDrive($endPoint);
    return $this->renderFileList();
  }

  /**
   * Get the drive contents.
   *
   * @param string $endPoint
   *   The endpoint we want the content from.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   * @throws \Microsoft\Graph\Exception\GraphException
   */
  private function getDrive(string $endPoint): void {
    $this->drive = $this->graphService->getGraphData($endPoint);
  }

  /**
   * Render the list of files and folders.
   *
   * @param int $limit
   *   The amount of items to show.
   *
   * @return array
   *   The render array for the files and folders.
   */
  protected function renderFileList(int $limit = 5): array {
    $listItems = [];
    if ($this->drive) {
      $values = array_slice($this->drive['value'], 0, $limit);

      foreach ($values as $value) {
        if (isset($value['file'])) {
          $listItems[] = [
            '#theme' => 'o365_onedrive_files',
            '#fileName' => pathinfo($value['name'], PATHINFO_FILENAME),
            '#fileUrl' => (isset($value['webUrl'])) ? $value['webUrl'] : $value['remoteItem']['webDavUrl'],
            '#fileExtension' => $this->getType($value['file']['mimeType']),
          ];
        }
        else {
          $url = FALSE;
          if (isset($value['webUrl'])) {
            $url = $value['webUrl'];
          }
          elseif (isset($value['remoteItem']['webDavUrl'])) {
            $url = $value['remoteItem']['webDavUrl'];
          }

          if ($url) {
            $listItems[] = [
              '#theme' => 'o365_onedrive_files',
              '#fileName' => pathinfo($value['name'], PATHINFO_FILENAME),
              '#fileUrl' => $url,
              '#fileExtension' => 'folder',
            ];
          }
        }
      }
    }

    return [
      '#theme' => 'o365_onedrive_results',
      '#results' => [
        '#theme' => 'item_list',
        '#items' => $listItems,
      ],
      '#attached' => [
        'library' => [
          'o365/icons',
          'o365_onedrive/onedrive',
        ],
      ],
      '#cache' => [
        'contexts' => [
          'user',
        ],
      ],
    ];
  }

  /**
   * Get the type of mime used for styling.
   *
   * @param string $mimeType
   *   The mimetype of the file.
   *
   * @return string
   *   The type.
   */
  private function getType(string $mimeType): string {
    if (str_starts_with($mimeType, 'video')) {
      return 'video';
    }

    if (str_starts_with($mimeType, 'image')) {
      return 'photo';
    }

    if (str_starts_with($mimeType, 'audio')) {
      return 'photo';
    }

    return (new MimeTypes())->getExtension($mimeType);
  }

}
