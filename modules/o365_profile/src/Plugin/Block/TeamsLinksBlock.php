<?php

namespace Drupal\o365_profile\Plugin\Block;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\externalauth\Authmap;
use Drupal\o365\Block\O365UncachedBlockBase;
use Drupal\o365\GraphService;
use Drupal\o365_profile\O365ProfileTeamsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Teams Links' block.
 *
 * @Block(
 *   id = "o365_profile_teams_links",
 *   admin_label = @Translation("Microsoft 365 - Teams Links"),
 *   category = @Translation("Microsoft 365")
 * )
 */
final class TeamsLinksBlock extends O365UncachedBlockBase implements ContainerFactoryPluginInterface {

  /**
   * The o365_profile.teams service.
   *
   * @var \Drupal\o365_profile\O365ProfileTeamsService
   */
  protected O365ProfileTeamsService $o365ProfileTeams;

  /**
   * The externalauth authmap service.
   *
   * @var \Drupal\externalauth\Authmap
   */
  protected Authmap $authmap;

  /**
   * Constructs a new TeamsLinksBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\o365\GraphService $graphService
   *   The o365 Graph service.
   * @param \Drupal\o365_profile\O365ProfileTeamsService $o365_profile_teams
   *   The o365_profile.teams service.
   * @param \Drupal\externalauth\Authmap $authmap
   *   The externalauth authmap service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GraphService $graphService, O365ProfileTeamsService $o365_profile_teams, Authmap $authmap) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $graphService);
    $this->o365ProfileTeams = $o365_profile_teams;
    $this->authmap = $authmap;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('o365.graph'), $container->get('o365_profile.teams'), $container->get('externalauth.authmap'));
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $user = \Drupal::routeMatch()->getParameter('user');
    $build = [];
    if ($user && $this->authmap->get($user->id(), 'o365_sso')) {
      $build['#attached'] = [
        'library' => [
          'o365/icons',
          'o365_profile/teams_block',
        ],
      ];
      $build['#cache'] = [
        'tags' => [
          'user:' . $user->id(),
        ],
      ];

      $call = $this->o365ProfileTeams->generateTeamsLink($user, 'call');
      $chat = $this->o365ProfileTeams->generateTeamsLink($user, 'chat');
      $video = $this->o365ProfileTeams->generateTeamsLink($user, 'video');

      if ($call) {
        $build['o365_profile_teams_call'] = $call;
      }

      if ($chat) {
        $build['o365_profile_teams_chat'] = $chat;
      }

      if ($video) {
        $build['o365_profile_teams_video'] = $video;
      }
    }

    return $build;
  }

}
