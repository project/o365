<?php

namespace Drupal\o365_profile\Plugin\Block;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\o365\Block\O365BlockBase;
use Drupal\o365\GraphService;
use Drupal\o365\PersonaRenderService;
use Drupal\o365_profile\O365ProfileGetDataService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Persona' block.
 *
 * @Block(
 *   id = "o365_persona",
 *   admin_label = @Translation("Persona"),
 *   category = @Translation("Microsoft 365")
 * )
 */
final class PersonaBlock extends O365BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\o365\GraphService definition.
   *
   * @var \Drupal\o365_profile\O365ProfileGetDataService
   */
  protected O365ProfileGetDataService $o365ProfileGraph;

  /**
   * The persona render service.
   *
   * @var \Drupal\o365\PersonaRenderService
   */
  protected PersonaRenderService $personaRenderService;

  /**
   * Constructs a new PersonaBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\o365\GraphService $graphService
   *   The o365 Graph service.
   * @param \Drupal\o365_profile\O365ProfileGetDataService $o365ProfileGraph
   *   The GraphService definition.
   * @param \Drupal\o365\PersonaRenderService $personaRenderService
   *   The persona render service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected GraphService $graphService, O365ProfileGetDataService $o365ProfileGraph, PersonaRenderService $personaRenderService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $graphService);
    $this->o365ProfileGraph = $o365ProfileGraph;
    $this->personaRenderService = $personaRenderService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('o365.graph'), $container->get('o365_profile.get_data'), $container->get('o365.profile_render'));
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['show_photo'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the users photo'),
      '#default_value' => $config['show_photo'] ?? FALSE,
    ];

    $form['image_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Image size (square)'),
      '#default_value' => $config['image_size'] ?? '64X64',
      '#options' => [
        '24x24' => $this->t('24 pixels square'),
        '48x48' => $this->t('48 pixels square'),
        '64x64' => $this->t('64 pixels square'),
        '96x96' => $this->t('96 pixels square'),
        '120x120' => $this->t('120 pixels square'),
        '240x240' => $this->t('240 pixels square'),
        '360x360' => $this->t('360 pixels square'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="settings[show_photo]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['show_presence'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the users presence'),
      '#default_value' => $config['show_presence'] ?? FALSE,
      '#states' => [
        'visible' => [
          ':input[name="settings[show_photo]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['show_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the users display name'),
      '#default_value' => $config['show_name'] ?? FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['show_photo'] = $values['show_photo'];
    $this->configuration['image_size'] = $values['image_size'];
    $this->configuration['show_presence'] = $values['show_presence'];
    $this->configuration['show_name'] = $values['show_name'];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException|\GuzzleHttp\Exception\GuzzleException
   */
  public function build(): array {
    $config = $this->getConfiguration();
    $configuredImageSize = (isset($config['image_size'])) ? $config['image_size'] : '96x96';
    $userData = $this->o365ProfileGraph->getProfileData(NULL, $configuredImageSize);

    $build = $this->personaRenderService->renderPersona($userData, 'small');
    // Add some extra config we need in the template.
    $imageSize = explode('X', $configuredImageSize);
    $config['size'] = $imageSize[0];
    $build['#config'] = $config;

    $build['#cache'] = [
      'tags' => ['user:' . $userData['currentUserId']],
      'context' => ['user'],
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return Cache::mergeTags(parent::getCacheTags(), ['user:' . $this->o365ProfileGraph->getProfileData()['currentUserId']]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return Cache::mergeContexts(parent::getCacheContexts(), ['user']);
  }

}
