<?php

namespace Drupal\o365_profile\Plugin\views\field;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\o365_profile\O365ProfileTeamsService;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Render\ViewsRenderPipelineMarkup;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generate a views field that links to a Teams call or chat.
 *
 * @package Drupal\o365_profile\Plugin\views\field
 *
 * @ViewsField("o365_profile_teams_call")
 */
final class CallViaTeamsViewsField extends FieldPluginBase {

  /**
   * The teams service. Used for generating links.
   *
   * @var \Drupal\o365_profile\O365ProfileTeamsService
   */
  protected O365ProfileTeamsService $teamsService;

  /**
   * CallViaTeamsViewsField constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\o365_profile\O365ProfileTeamsService $teamsService
   *   The o365 profile teams service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, O365ProfileTeamsService $teamsService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->teamsService = $teamsService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      // Add your service here to pass an instance to the constructor.
      $container->get('o365_profile.teams')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['link_type'] = ['default' => 'chat'];
    $options['link_text'] = ['default' => 'Chat via Teams'];
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    $form['link_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the link type'),
      '#default_value' => $this->options['link_type'],
      '#description' => $this->t('This field determines the type of link that will be generated, either a chat, call of video call link.'),
      '#options' => [
        'chat' => $this->t('Chat'),
        'call' => $this->t('Call'),
        'video' => $this->t('Video call'),
      ],
    ];

    $form['link_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#default_value' => $this->options['link_text'],
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values): MarkupInterface|bool|array|string|ViewsRenderPipelineMarkup {
    /** @var \Drupal\user\Entity\User $user */
    $user = $values->_entity;
    return $this->teamsService->generateTeamsLink($user, $this->options['link_type'], $this->options['link_text'], TRUE);
  }

}
