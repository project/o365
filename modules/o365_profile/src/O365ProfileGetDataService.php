<?php

namespace Drupal\o365_profile;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\o365\GraphService;
use GuzzleHttp\Exception\ClientException;

/**
 * O365ProfileGetDataService service gives basic information about the user.
 */
class O365ProfileGetDataService {

  /**
   * The o365.graph service.
   *
   * @var \Drupal\o365\GraphService
   */
  protected GraphService $o365Graph;

  /**
   * The config of the SSO user module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $ssoUserConfig;

  /**
   * Constructs an O365ProfileGetDataService object.
   *
   * @param \Drupal\o365\GraphService $o365_graph
   *   The o365.graph service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(GraphService $o365_graph, ConfigFactory $configFactory) {
    $this->o365Graph = $o365_graph;
    $this->ssoUserConfig = $configFactory->get('o365_sso_user.settings');
  }

  /**
   * Method gives the basic current logged in user information.
   *
   * @param string|null $user_id
   *   The user ID or NULL.
   * @param string $photoSize
   *   The size of the user photo we need to retrieve.
   * @param bool $getPhoto
   *   If we want to retrieve the user photo.
   *
   * @return array
   *   The list of user data.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException|\Microsoft\Graph\Exception\GraphException
   */
  public function getProfileData(string|null $user_id = NULL, string $photoSize = '64x64', bool $getPhoto = TRUE): array {
    // Get the current user ID.
    $profileData['currentUserId'] = $user_id ?? $this->o365Graph->getCurrentUserId();
    $profileData['imageSrc'] = FALSE;
    $endPoint = '/users/' . $profileData['currentUserId'];

    // Get the presence data.
    $profileData['presenceData'] = $this->o365Graph->getGraphData($endPoint . '/presence');

    // Get other user data if entered.
    if (!empty($this->ssoUserConfig->get('graph_data_selected_fields'))) {
      $endPoint .= '?$select=' . str_replace(' ', '', $this->ssoUserConfig->get('graph_data_selected_fields'));
      // Add some required fields, they can be duplicate from the selected
      // fields, but they are only returned once.
      $endPoint .= ',displayName,id,givenName,surname';
    }

    $profileData['userData'] = $this->o365Graph->getGraphData($endPoint);
    $profileData['initials'] = ($profileData['userData']) ? $this->getUserInitials($profileData['userData']) : 'A';

    if ($getPhoto) {
      try {
        // For smaller image sizes we can make the 64x64 a variable.
        if ($photoSize === '24x24') {
          $photoSize = '48x48';
        }

        $imageData = $this->o365Graph->getGraphData('/users/' . $profileData['currentUserId'] . '/photos/' . strtolower($photoSize) . '/$value', 'GET', TRUE);
        if (!empty($imageData)) {
          $profileData['imageData'] = $imageData;
          $profileData['imageMeta'] = $this->o365Graph->getGraphData('/users/' . $profileData['currentUserId'] . '/photos/' . strtolower($photoSize));
          $profileData['imageBase64'] = base64_encode($profileData['imageData']);
          $profileData['imageSrc'] = 'data:' . $profileData['imageMeta']['@odata.mediaContentType'] . ';base64,' . base64_encode($profileData['imageData']);
        }
      }
      catch (ClientException $exception) {
        // @todo What are we going to do with a empty profile picture.
        // Graph api gives a error if profile picture is never set by the user.
        // Set an empty string if profile picture is empty.
        $profileData['imageSrc'] = '';
      }
    }

    return $profileData;
  }

  /**
   * Get the users initials from the profile data.
   *
   * @param array $profileData
   *   The users profile data.
   *
   * @return string
   *   The users initial.
   */
  public function getUserInitials(array $profileData): string {
    $name = $profileData['displayName'];
    $initials = explode(' ', $name);

    if (count($initials) > 1) {
      $first = implode(" ", $initials);
      $last = array_pop($initials);

      return strtoupper($first[0] . $last[0]);
    }

    return strtoupper($initials[0][0]);
  }

}
