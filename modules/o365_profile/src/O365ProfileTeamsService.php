<?php

namespace Drupal\o365_profile;

use Drupal\Core\Render\RendererInterface;
use Drupal\user\UserInterface;

/**
 * The service too connect Teams with profiles.
 */
final class O365ProfileTeamsService {

  /**
   * Our class contructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer interface.
   */
  public function __construct(
    private readonly RendererInterface $renderer,
  ) {}

  /**
   * Generate a link to interact with the user on Teams.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user object.
   * @param string $type
   *   The type of link to generate.
   * @param mixed $text
   *   The text we want to use in the link, or FALSE for the default text.
   * @param bool $render
   *   If we want the render array rendered or not.
   *
   * @return array|bool|\Drupal\Component\Render\MarkupInterface
   *   The render array or FALSE.
   */
  public function generateTeamsLink(UserInterface $user, string $type = 'chat', $text = FALSE, $render = FALSE) {
    $config = \Drupal::config('o365_profile.teams_links_config');
    $showLink = $config->get('ms_teams_' . $type);

    if (!empty($showLink)) {
      // Based on the type of the call we need a separate url and text.
      switch ($type) {
        case 'call':
          $icon = 'TransferCall';
          $url = 'https://teams.microsoft.com/l/call/0/0?users=' . $user->getEmail();
          if (!$text) {
            $text = t('Call with Teams');
          }
          break;

        case 'video':
          $icon = 'Video';
          $url = 'https://teams.microsoft.com/l/call/0/0?users=' . $user->getEmail() . '&Withvideo=true';
          if (!$text) {
            $text = t('Video call with Teams');
          }
          break;

        default:
          $icon = 'ChatInviteFriend';
          $url = 'https://teams.microsoft.com/l/chat/0/0?users=' . $user->getEmail();
          if (!$text) {
            $text = t('Chat with Teams');
          }
      }

      // Generate the render array.
      $renderArray = [
        '#theme' => 'o365_profile_teams_link',
        '#url' => $url,
        '#text' => $text,
        '#link_classes' => ['type--' . $type],
        '#container_classes' => [
          'teams-link',
          'teams-link--' . $type,
        ],
        '#type' => $type,
        '#icon' => $icon,
      ];

      if ($render) {
        return $this->renderer->render($renderArray);
      }

      return $renderArray;
    }

    return FALSE;
  }

}
