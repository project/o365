<?php

namespace Drupal\o365_outlook_calendar\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\o365\Block\O365UncachedBlockBase;
use Drupal\o365\GraphService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides an add node to calendar block.
 *
 * @Block(
 *   id = "o365_outlook_calendar_add_to_calendar",
 *   admin_label = @Translation("Microsoft 365 - Add event to calendar"),
 *   category = @Translation("Microsoft 365")
 * )
 */
final class AddToCalendarBlock extends O365UncachedBlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $moduleConfig;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected ?Request $request;

  /**
   * Constructs a new AddToCalendarBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\o365\GraphService $graphService
   *   The o365 Graph service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GraphService $graphService, ConfigFactoryInterface $configFactory, RequestStack $requestStack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $graphService);
    $this->moduleConfig = $configFactory->get('o365_outlook_calendar.settings');
    $this->request = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('o365.graph'),
      $container->get('config.factory'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->request->get('node');
    if ($node) {
      $enabled = $this->moduleConfig->get('enabled');
      if ($enabled && $node->bundle() === $this->moduleConfig->get('content_type')) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->request->get('node');

    if ($node) {
      $build['ajax_link'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['card__block text-center']],
      ];
      $build['ajax_link']['link'] = [
        '#type' => 'link',
        '#title' => $this->t('Save this event in my Outlook Calendar'),
        '#attached' => ['library' => ['core/drupal.ajax']],
        '#attributes' => ['class' => ['use-ajax btn btn-accent btn-md btn-raised waves-effect waves-btn waves-light']],
        '#url' => Url::fromRoute('o365_outlook_calendar.save_ajax_callback', ['nid' => $node->id()]),
        '#prefix' => '<div id="o365-calendar-ajax-link">',
        '#suffix' => '</div>',
      ];
    }

    return $build;
  }

}
