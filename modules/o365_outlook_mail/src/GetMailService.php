<?php

namespace Drupal\o365_outlook_mail;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\o365\GraphService;

/**
 * This service retrieves emails for the currently logged in user.
 */
class GetMailService implements GetMailServiceInterface {

  /**
   * Drupal\o365\GraphService definition.
   *
   * @var \Drupal\o365\GraphService
   */
  protected $o365Graph;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new GetMailService object.
   *
   * @param \Drupal\o365\GraphService $o365_graph
   *   The GraphService definition.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter service.
   */
  public function __construct(GraphService $o365_graph, DateFormatterInterface $dateFormatter) {
    $this->o365Graph = $o365_graph;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public function getMail($limit = 10, array $fields = []) {
    $select = '';
    if (!empty($fields)) {
      $select = '$select=' . implode(',', $fields) . '&';
    }

    $mailData = $this->o365Graph->getGraphData("/me/mailFolders('Inbox')/messages?" . $select . '$top=' . $limit);

    if (isset($mailData['value']) && !empty($mailData['value'])) {
      return $mailData['value'];
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getUnreadMail($limit = 5, array $fields = []) {
    $select = '';
    if (!empty($fields)) {
      $select = '$select=' . implode(',', $fields) . '&';
    }

    $mailData = $this->o365Graph->getGraphData('/me/mailFolders/inbox/messages?$filter=isRead eq false&' . $select . '$top=' . $limit);

    if (isset($mailData['value']) && !empty($mailData['value'])) {
      return $mailData['value'];
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function generateMailList(array $mailData, $type = 'page') {
    $items = [];

    // Determine theme function.
    $themeFunction = 'o365_outlook_mail_list';
    if ($type === 'block') {
      $themeFunction = 'o365_outlook_mail_list_block';
    }

    $subjectUrlOptions = [
      'attributes' => [
        'target' => '_blank',
        'title' => new TranslatableMarkup('This link opens in a new window'),
      ],
    ];

    foreach ($mailData as $mail) {
      $subjectUrl = Url::fromUri($mail['webLink'], $subjectUrlOptions);
      $webLink = Link::fromTextAndUrl(t('Read this email on Outlook.com'), $subjectUrl);

      // We want to format the date differently for older mails.
      $timestamp = strtotime($mail['receivedDateTime']);
      $oneweekago = strtotime('-1 week');
      if ($oneweekago <= $timestamp) {
        $date = $this->dateFormatter->format($timestamp, 'o365_outlook_mail_day');
      }
      else {
        $date = $this->dateFormatter->format($timestamp, 'o365_outlook_mail_date');
      }

      $items[] = [
        '#theme' => 'o365_outlook_mail_item',
        '#from' => $mail['from']['emailAddress']['name'],
        '#subject' => $mail['subject'],
        '#receivedDateTime' => $date,
        '#bodyPreview' => $mail['bodyPreview'],
        '#isRead' => $mail['isRead'],
        '#webLink' => $webLink,
      ];
    }

    $build['content'] = [
      '#theme' => $themeFunction,
      '#readmore_class' => 'button--primary button',
      '#list' => [
        '#theme' => 'item_list',
        '#items' => $items,
        '#attributes' => [
          'class' => ['o365-outlook-mails'],
        ],
        '#wrapper_attributes' => [
          'class' => [
            'card__body',
          ],
        ],
        '#attached' => [
          'library' => [
            'o365_outlook_mail/o365_outlook_mail',
            'o365/icons',
          ],
        ],
      ],
      '#cache' => [
        'contexts' => [
          'user',
        ],
      ],
    ];

    return $build;
  }

}
