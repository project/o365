<?php

namespace Drupal\o365_outlook_mail;

/**
 * Interface for the GetMailService class.
 */
interface GetMailServiceInterface {

  /**
   * Get the users mails.
   *
   * @param int $limit
   *   The number of mails to get.
   * @param array $fields
   *   The fields we want to get. If empty all fields will be returned.
   *
   * @return array|bool
   *   The list of mails or FALSE if no mails.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   * @throws \Microsoft\Graph\Exception\GraphException
   */
  public function getMail($limit = 10, array $fields = []);

  /**
   * Get the users unread mails.
   *
   * @param int $limit
   *   The number of mails to get.
   * @param array $fields
   *   The fields we want to get. If empty all fields will be returned.
   *
   * @return array|bool
   *   The list of mails or FALSE if no mails.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   * @throws \Microsoft\Graph\Exception\GraphException
   */
  public function getUnreadMail($limit = 5, array $fields = []);

  /**
   * Generate a rendered list of emails.
   *
   * @param array $mailData
   *   The mail data to process.
   * @param string $type
   *   The type of output we need so we can differentiate between pages/blocks.
   *
   * @return array
   *   The render array with the list of mails.
   */
  public function generateMailList(array $mailData, $type = 'page');

}
