<?php

namespace Drupal\o365_sso_user\EventSubscriber;

use Drupal\externalauth\Event\ExternalAuthEvents;
use Drupal\externalauth\Event\ExternalAuthLoginEvent;
use Drupal\o365_sso_user\SsoUserSync;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * The event subscriber for SSO logins.
 *
 * @package Drupal\o365_sso\EventSubscriber
 */
class UserLoginEventSubscriber implements EventSubscriberInterface {

  /**
   * The SSO user sync service.
   *
   * @var \Drupal\o365_sso_user\SsoUserSync
   */
  protected $ssoUserSync;

  /**
   * UserLoginEventSubscriber constructor.
   *
   * @param \Drupal\o365_sso_user\SsoUserSync $ssoUserSync
   *   The SSO User sync service.
   */
  public function __construct(SsoUserSync $ssoUserSync) {
    $this->ssoUserSync = $ssoUserSync;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ExternalAuthEvents::LOGIN][] = ['updateUserData'];
    return $events;
  }

  /**
   * Update a users data with data dfrom the Graph API.
   *
   * @param \Drupal\externalauth\Event\ExternalAuthLoginEvent $event
   *   The ExternalAuthLoginEvent event containing the user.
   */
  public function updateUserData(ExternalAuthLoginEvent $event) {
    if ($event->getProvider() === 'o365_sso') {
      $this->ssoUserSync->syncUserData($event->getAccount());
    }
  }

}
