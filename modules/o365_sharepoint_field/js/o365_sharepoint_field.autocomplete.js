/* eslint-disable func-names, comma-dangle, strict */

(($, Drupal) => {
  'use strict';

  Drupal.behaviors.o365SharePointSearchAutocomplete = {
    attach: function attach(context) {
      $(context)
        .find('.o365-sharepoint-autocomplete')
        .on('autocompleteselect', function (event, data) {
          const hitIdField = $(context).find('.o365-sharepoint-hit-id');
          const driveIdField = $(context).find('.o365-sharepoint-drive-id');

          // Set the correct field values.
          hitIdField.val(data.item.hitId);
          driveIdField.val(data.item.driveId);
        });
    },
  };
})(jQuery, Drupal, drupalSettings);
