<?php

namespace Drupal\o365_sharepoint_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Defines the 'field_sharepoint_search_link' field widget.
 *
 * @FieldWidget(
 *   id = "sharepoint_search_link",
 *   label = @Translation("Microsoft 365 - SharePoint Search field"),
 *   field_types = {"sharepoint_search_link"},
 * )
 */
class SharePointSearchWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\o365\AuthenticationService $authenticationService */
    $authenticationService = \Drupal::service('o365.authentication');

    if ($authenticationService->checkForOfficeLogin()) {
      $form['#attached']['library'][] = 'core/drupal.ajax';
      $form['#attached']['library'][] = 'o365_sharepoint_field/autocomplete';

      $value = $items->getValue();
      $sharepointSearch = $value[0]['sharepoint_file_name'] ?? '';
      $hitId = $value[0]['sharepoint_hit_id'] ?? '';
      $driveId = $value[0]['sharepoint_drive_id'] ?? '';
      $webUrl = $value[0]['sharepoint_web_url'] ?? '';

      $element['sharepoint_file_name'] = [
        '#type' => 'textfield',
        '#title' => $element['#title'],
        '#maxlength' => 255,
        '#default_value' => $sharepointSearch,
        '#autocomplete_route_name' => 'o365_sharepoint_field.autocomplete.search_sharepoint',
        '#attributes' => [
          'class' => ['o365-sharepoint-autocomplete'],
        ],
      ];

      $element['sharepoint_hit_id'] = [
        '#type' => 'hidden',
        '#title' => $this->t('HitId'),
        '#maxlength' => 255,
        '#default_value' => $hitId,
        '#attributes' => [
          'class' => ['o365-sharepoint-hit-id'],
        ],
      ];

      $element['sharepoint_drive_id'] = [
        '#type' => 'hidden',
        '#title' => $this->t('DriveId'),
        '#maxlength' => 255,
        '#default_value' => $driveId,
        '#attributes' => [
          'class' => ['o365-sharepoint-drive-id'],
        ],
      ];

      $element['sharepoint_web_url'] = [
        '#type' => 'hidden',
        '#title' => $this->t('Web url'),
        '#maxlength' => 6000,
        '#default_value' => $webUrl,
      ];
    }
    else {
      $element['sharepoint_message'] = [
        '#type' => 'markup',
        '#markup' => $this->t('You are not logged in with Office. This field is disabled because of that.'),
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return isset($violation->arrayPropertyPath[0]) ? $element[$violation->arrayPropertyPath[0]] : $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    if ($values[0]['sharepoint_drive_id'] !== '' && $values[0]['sharepoint_hit_id'] !== '') {
      // Filling the web_url field based on drive_id and hit_id.
      $graph = \Drupal::service('o365.graph');
      /** @var \Drupal\o365\AuthenticationService $authenticationService */
      $authenticationService = \Drupal::service('o365.authentication');
      if ($authenticationService->checkForOfficeLogin()) {
        $sharepointImageData = $graph->getGraphData('/drives/' . $values[0]['sharepoint_drive_id'] . '/items/' . $values[0]['sharepoint_hit_id']);
        $sharepointUrl = $sharepointImageData['webUrl'];
        $values[0]['sharepoint_web_url'] = $sharepointUrl;
      }
    }

    return $values;
  }

}
