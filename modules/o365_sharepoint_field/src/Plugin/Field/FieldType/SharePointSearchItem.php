<?php

namespace Drupal\o365_sharepoint_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'sharepoint_search_link' field type.
 *
 * @FieldType(
 *   id = "sharepoint_search_link",
 *   label = @Translation("SharePoint search field"),
 *   description = @Translation("Search for, and link to, files in Sharepoint."),
 *   category = @Translation("Microsoft 365"),
 *   default_widget = "sharepoint_search_link",
 *   default_formatter = "sharepoint_search_link"
 * )
 *
 * @property string $sharepoint_file_name
 */
class SharePointSearchItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if ($this->sharepoint_file_name !== NULL) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['sharepoint_file_name'] = DataDefinition::create('string')
      ->setLabel(t('File name'));
    $properties['sharepoint_hit_id'] = DataDefinition::create('string')
      ->setLabel(t('Hit ID'));
    $properties['sharepoint_drive_id'] = DataDefinition::create('string')
      ->setLabel(t('Drive ID'));
    $properties['sharepoint_web_url'] = DataDefinition::create('string')
      ->setLabel(t('Web URL'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'sharepoint_file_name' => [
          'type' => 'varchar',
          'length' => 2048,
        ],
        'sharepoint_hit_id' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'sharepoint_drive_id' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'sharepoint_web_url' => [
          'type' => 'varchar',
          'length' => 2048,
        ],
      ],
    ];
  }

}
