<?php

namespace Drupal\o365_links\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\o365_links\O365CacheInvalidator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure o365_links settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The list of Microsoft 365 applications.
   *
   * @var array
   */
  protected array $officeLinks;

  /**
   * The cache tag invalidator service.
   *
   * @var \Drupal\o365_links\O365CacheInvalidator
   */
  protected O365CacheInvalidator $cacheInvalidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $service = $container->get('o365_links.constants');
    $instance->officeLinks = $service->getApplications();

    $cacheService = $container->get('o365_links.cache_invalidator');
    $instance->cacheInvalidator = $cacheService;

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'o365_links_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['o365_links.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['#attached']['library'][] = 'o365/icons';
    $form['#attached']['library'][] = 'o365_links/backend';

    $form['new_window'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open links in a new window'),
      '#default_value' => $this->config('o365_links.settings')
        ->get('new_window'),
    ];

    $form['applications'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Microsoft 365 applications'),
    ];

    foreach ($this->officeLinks as $name => $value) {
      $configKey = 'o365_links_' . $name;
      $form['applications'][$configKey] = [
        '#type' => 'checkbox',
        '#title' => Markup::create('<div class="o365-link-inline ms-BrandIcon--icon48 ms-BrandIcon--' . $name . '"></div><span class="name">' . $value['label'] . '</span>'),
        '#default_value' => $this->config('o365_links.settings')
          ->get($configKey),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('o365_links.settings');
    $config->set('new_window', $form_state->getValue('new_window'));

    foreach ($this->officeLinks as $name => $value) {
      $configKey = 'o365_links_' . $name;
      $config->set($configKey, $form_state->getValue($configKey));
    }

    $config->save();
    $this->cacheInvalidator->invalidateBlockCache();
    parent::submitForm($form, $form_state);
  }

}
