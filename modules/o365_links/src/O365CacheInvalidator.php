<?php

namespace Drupal\o365_links;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Create a custom cache invalidator.
 */
class O365CacheInvalidator {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected CacheTagsInvalidatorInterface $cacheTagsInvalidator;

  /**
   * Constructs an O365CacheInvalidator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CacheTagsInvalidatorInterface $cache_tags_invalidator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * Invalidate the cache tags for our blocks.
   */
  public function invalidateBlockCache(): void {
    $blockIds = \Drupal::entityQuery('block')
      ->condition('plugin', 'o365_links_block')
      ->accessCheck()
      ->execute();

    if (!empty($blockIds)) {
      $tags = [];
      foreach ($blockIds as $block_id) {
        $block = $this->entityTypeManager->getStorage('block')->load($block_id);
        if ($block) {
          $tags[] = $block->getCacheTagsToInvalidate();
        }
      }

      $tags = array_merge([], ...$tags);
      if (!empty($tags)) {
        $this->cacheTagsInvalidator->invalidateTags($tags);
      }
    }
  }

}
