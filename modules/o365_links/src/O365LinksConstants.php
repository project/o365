<?php

namespace Drupal\o365_links;

/**
 * Some constants we need in the links module.
 */
class O365LinksConstants {

  /**
   * This method generates a list of office applications and their links.
   *
   * @return array
   *   The list of applications.
   */
  public function getApplications(): array {
    return [
      'word' => [
        'label' => 'Word',
        'url' => 'https://www.office.com/launch/word',
      ],
      'excel' => [
        'label' => 'Excel',
        'url' => 'https://www.office.com/launch/excel',
      ],
      'powerpoint' => [
        'label' => 'Powerpoint',
        'url' => 'https://www.office.com/launch/powerpoint',
      ],
      'outlook' => [
        'label' => 'Outlook',
        'url' => 'https://outlook.office.com',
      ],
      'onenote' => [
        'label' => 'OneNote',
        'url' => 'https://www.office.com/launch/onenote',
      ],
      'office' => [
        'label' => 'Office',
        'url' => 'https://www.office.com',
      ],
      'teams' => [
        'label' => 'Teams',
        'url' => 'https://aka.ms/mstfw',
      ],
    ];
  }

}
