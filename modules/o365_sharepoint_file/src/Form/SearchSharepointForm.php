<?php

namespace Drupal\o365_sharepoint_file\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a o365_files form.
 */
class SearchSharepointForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'o365_sharepoint_search_file';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $keyword = \Drupal::request()->get('keyword');
    $form['keyword'] = [
      '#title' => $this->t('Search for files'),
      '#type' => 'textfield',
      '#title_display' => 'invisible',
      '#default_value' => ($keyword) ?: '',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search files'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('o365_sharepoint_file.search', ['keyword' => $form_state->getValue('keyword')]);
  }

}
