<?php

namespace Drupal\o365_sharepoint_file\Plugin\Block;

use Drupal\o365\Block\O365BlockBase;

/**
 * Provides a 'Sharepoint File Search' block.
 *
 * @Block(
 *   id = "o365_sharepoint_file",
 *   admin_label = @Translation("Sharepoint file search"),
 *   category = @Translation("Microsoft 365")
 * )
 */
class SharepointSearchBlock extends O365BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return \Drupal::formBuilder()
      ->getForm('Drupal\o365_sharepoint_file\Form\SearchSharepointForm');
  }

}
