<?php

namespace Drupal\o365_groups\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\group\Entity\Group;
use Drupal\o365_groups\GroupFilesService;
use Drupal\o365_groups\GroupsService;
use Mimey\MimeTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Microsoft 365 - Groups routes.
 */
final class O365GroupsFilesController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected ?Request $request;

  /**
   * The group file service.
   *
   * @var \Drupal\o365_groups\GroupFilesService
   */
  protected GroupFilesService $groupFilesService;

  /**
   * The mimey mimetypes.
   *
   * @var \Mimey\MimeTypes
   */
  protected MimeTypes $mimey;

  /**
   * The groups service.
   *
   * @var \Drupal\o365_groups\GroupsService
   */
  protected GroupsService $groupsService;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\o365_groups\GroupFilesService $groupFilesService
   *   The service to generate the group files arrays.
   * @param \Drupal\o365_groups\GroupsService $groupsService
   *   The groups service.
   */
  public function __construct(DateFormatterInterface $date_formatter, RequestStack $requestStack, GroupFilesService $groupFilesService, GroupsService $groupsService) {
    $this->dateFormatter = $date_formatter;
    $this->request = $requestStack->getCurrentRequest();
    $this->groupFilesService = $groupFilesService;
    $this->groupsService = $groupsService;
    $this->mimey = new MimeTypes();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('date.formatter'), $container->get('request_stack'), $container->get('o365_groups.files'), $container->get('o365_groups.groups'));
  }

  /**
   * Builds the response.
   */
  public function build(): array {
    $groupId = $this->request->get('group');
    if ($groupId) {
      /** @var \Drupal\group\Entity\Group $group */
      $group = Group::load($groupId);

      if ($group) {
        $teamsIdField = $this->groupsService->getTeamsUuidForGroup($group->id(), TRUE);

        if (!empty($teamsIdField)) {
          // Get sort values from url.
          $sort = $this->request->get('sort') ?? 'asc';
          $order = $this->request->get('order') ?? 'name';

          $childrenData = $this->groupFilesService->getOrderedGroupFiles($teamsIdField, $order, $sort);
          // Create the table header.
          $header = [
            [
              'data' => t('Type'),
              'field' => 'type',
              'class' => [RESPONSIVE_PRIORITY_LOW, 'o365-groups-file-type'],
            ],
            [
              'data' => t('Name'),
              'sort' => 'asc',
              'field' => 'name',
              'class' => ['o365-groups-file-name'],
            ],
            [
              'data' => t('Modified'),
              'field' => 'lastModifiedDateTime',
              'class' => ['o365-groups-file-modified'],
            ],
            [
              'data' => t('Modified by'),
              'field' => 'lastModifiedBy',
              'class' => ['o365-groups-file-modified-by'],
            ],
          ];
          $rows = [];

          // Loop through the items to generate the table rows.
          if (!empty($childrenData)) {
            /** @var \Microsoft\Graph\Model\DriveItem $child */
            foreach ($childrenData as $child) {
              // Format the modified date.
              $createdTs = $child->getCreatedDateTime()->getTimestamp();
              $oneweekago = strtotime("-1 week");

              if ($oneweekago <= $createdTs) {
                $date = t('@time ago', ['@time' => $this->dateFormatter->formatTimeDiffSince($createdTs, ['granularity' => 1])]);
              }
              else {
                $date = $this->dateFormatter->format($createdTs, 'o365_groups_date');
              }

              // Generate the url.
              $linkTitle = new TranslatableMarkup('This link opens in a new window');
              $url = Url::fromUri($child->getWebUrl(), [
                'attributes' => [
                  'target' => '_blank',
                  'title' => $linkTitle,
                ],
              ]);
              $link = Link::fromTextAndUrl($child->getName(), $url);

              // Determine the file icon.
              if ($child->getFile()) {
                $mimeType = $this->mimey->getExtension($child->getFile()
                  ->getMimeType());
              }
              else {
                $mimeType = 'folder';
              }

              $rows[] = [
                'no_striping' => TRUE,
                'data' => [
                  [
                    'data' => [
                      '#theme' => 'o365_brandicon',
                      '#type' => $mimeType,
                      '#size' => 48,
                    ],
                  ],
                  $link,
                  $date,
                  $child->getLastModifiedBy()->getUser()->getDisplayName(),
                ],
              ];
            }
          }

          $build['content'] = [
            '#theme' => 'o365_groups_file_list_container',
            '#table' => [
              '#theme' => 'table',
              '#rows' => $rows,
              '#header' => $header,
              '#empty' => t('There are no documents in the group.'),
            ],
            '#attached' => [
              'library' => [
                'o365/icons',
              ],
            ],
            '#cache' => [
              'tags' => ['group:' . $group->id()],
            ],
          ];
          return $build;
        }
      }
    }

    return [];
  }

  /**
   * Generate the documents page title.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The created title.
   */
  public function title(): TranslatableMarkup {
    $groupId = $this->request->get('group');
    if ($groupId) {
      /** @var \Drupal\group\Entity\Group $group */
      $group = Group::load($groupId);

      if ($group) {
        return $this->t('Documents for @group', ['@group' => $group->label()]);
      }
    }

    return $this->t('Documents');
  }

}
