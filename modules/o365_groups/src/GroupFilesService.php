<?php

namespace Drupal\o365_groups;

use Drupal\o365\GraphService;
use Microsoft\Graph\Model\DriveItem;
use Mimey\MimeTypes;

/**
 * Service that returns the list of files.
 */
class GroupFilesService {

  /**
   * The o365 graph service.
   *
   * @var \Drupal\o365\GraphService
   */
  protected GraphService $graphService;

  /**
   * The mimey mimetypes.
   *
   * @var \Mimey\MimeTypes
   */
  protected MimeTypes $mimey;

  /**
   * Constructs a GroupFilesService object.
   *
   * @param \Drupal\o365\GraphService $graphService
   *   The o365 graph service.
   */
  public function __construct(GraphService $graphService) {
    $this->graphService = $graphService;
    $this->mimey = new MimeTypes();
  }

  /**
   * Generate the list for the group files.
   *
   * @param string $group
   *   The group and team ID.
   * @param string $order
   *   The order field.
   * @param string $sort
   *   The sort order.
   *
   * @return array
   *   The list of rows.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   */
  public function getOrderedGroupFiles($group, $order = 'name', $sort = 'asc'): array {
    // We want to lowercase these values.
    $order = strtolower($order);
    $sort = strtolower($sort);

    // Explode the ID on the 3 pipes.
    $ids = explode('|||', $group);

    // Get the folder.
    $folderEndpoint = '/teams/' . $ids[0] . '/channels/' . $ids[1] . '/filesFolder';
    /** @var \Microsoft\Graph\Model\DriveItem $folderData */
    $folderData = $this->graphService->getGraphData($folderEndpoint, 'GET', FALSE, FALSE, DriveItem::class);

    // If the folder exists, get the files in that folder.
    if ($folderData) {
      $childrenEndpoint = '/drives/' . $folderData->getParentReference()
        ->getDriveId() . '/items/' . $folderData->getId() . '/children?$select=name,file,createdDateTime,lastModifiedDateTime,createdBy,webUrl,lastModifiedBy';

      // Add the endpoint order by string for name. Other columns are not yet
      // supported by Microsoft.
      if ($order === 'name') {
        $childrenEndpoint .= '&$orderby=' . $order . ' ' . $sort;
      }

      $childrenData = $this->graphService->getGraphData($childrenEndpoint, 'GET', FALSE, FALSE, DriveItem::class);

      if (!empty($childrenData)) {
        if ($order === 'name') {
          return $childrenData;
        }

        switch ($order) {
          case 'type':
            $childrenData = $this->orderByType($childrenData);
            break;

          case 'modified':
            $childrenData = $this->orderByModifiedDate($childrenData);
            break;

          case 'modified by':
            $childrenData = $this->orderByModifiedBy($childrenData);
            break;

        }

        // Order the array.
        ksort($childrenData);
        if ($sort === 'desc') {
          $childrenData = array_reverse($childrenData, TRUE);
        }

        return $childrenData;
      }
    }
    return [];
  }

  /**
   * Order the children by type.
   *
   * @param array $childrenData
   *   The children array.
   *
   * @return array
   *   The children woth added sort keys.
   */
  private function orderByType(array $childrenData): array {
    $return = [];
    /** @var \Microsoft\Graph\Model\DriveItem $child */
    foreach ($childrenData as $child) {
      // Determine the file icon.
      if ($child->getFile()) {
        $mimeType = $this->mimey->getExtension($child->getFile()
          ->getMimeType());
      }
      else {
        // If not a file, make the type zzzzz so it always comes last or first.
        $mimeType = 'zzzzzz';
      }

      $key = $mimeType . '_' . $child->getCreatedDateTime()->getTimestamp();
      $return[$key] = $child;
    }

    return $return;
  }

  /**
   * Order the children by modified date timestamp.
   *
   * @param array $childrenData
   *   The children array.
   *
   * @return array
   *   The children woth added sort keys.
   */
  private function orderByModifiedDate(array $childrenData): array {
    $return = [];
    /** @var \Microsoft\Graph\Model\DriveItem $child */
    foreach ($childrenData as $child) {
      $return[$child->getCreatedDateTime()->getTimestamp()] = $child;
    }

    return $return;
  }

  /**
   * Order the children by the "modified by" value.
   *
   * @param array $childrenData
   *   The children array.
   *
   * @return array
   *   The children woth added sort keys.
   */
  private function orderByModifiedBy(array $childrenData): array {
    $return = [];
    /** @var \Microsoft\Graph\Model\DriveItem $child */
    foreach ($childrenData as $child) {
      $key = $child->getLastModifiedBy()->getUser()
        ->getDisplayName() . '_' . $child->getCreatedDateTime()
        ->getTimestamp();
      $return[$key] = $child;
    }

    return $return;
  }

}
