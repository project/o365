<?php

namespace Drupal\o365_groups\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\Group;
use Drupal\o365_groups\GroupsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Access check class for the files controller.
 */
class O365GroupsFilesControllerAccessCheck implements AccessInterface {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected ?Request $request;

  /**
   * The groups service.
   *
   * @var \Drupal\o365_groups\GroupsService
   */
  protected GroupsService $groupsService;

  /**
   * Constructor for the O365GroupsFilesControllerAccessCheck class.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\o365_groups\GroupsService $groupsService
   *   The groups service.
   */
  public function __construct(RequestStack $requestStack, GroupsService $groupsService) {
    $this->request = $requestStack->getCurrentRequest();
    $this->groupsService = $groupsService;
  }

  /**
   * Access check method.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Whether the user has access.
   */
  public function access(AccountInterface $account): AccessResultForbidden|AccessResultAllowed {
    $group = $this->request->get('group');

    // Allowed to visit this page when there is a group and teams ID.
    if ($group) {
      if (is_string($group)) {
        $group = Group::load($group);
      }

      if ($group) {
        $teamsIdField = $this->groupsService->getTeamsUuidForGroup($group->id(), TRUE);
        if (!empty($teamsIdField)) {
          return AccessResult::allowed();
        }
      }
    }

    // Default is forbidden.
    return AccessResult::forbidden();
  }

}
