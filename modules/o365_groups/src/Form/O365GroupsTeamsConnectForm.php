<?php

namespace Drupal\o365_groups\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\o365\GraphService;
use Drupal\o365_groups\GroupsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create the form where we connect groups to MS Teams.
 */
final class O365GroupsTeamsConnectForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The graph service.
   *
   * @var \Drupal\o365\GraphService
   */
  protected GraphService $graphService;

  /**
   * The group service.
   *
   * @var \Drupal\o365_groups\GroupsService
   */
  protected GroupsService $groupsService;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Group.
   *
   * @var \Drupal\group\Entity\Group
   */
  protected mixed $group;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * Constructs a new O365GroupsTeamsConnectForm form.
   *
   * @param \Drupal\o365\GraphService $graphService
   *   The Microsoft 365 connector graph service.
   * @param \Drupal\o365_groups\GroupsService $groupsService
   *   The Microsoft 365 connector group service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route matcher.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(GraphService $graphService, GroupsService $groupsService, RouteMatchInterface $routeMatch, Connection $connection) {
    $this->graphService = $graphService;
    $this->groupsService = $groupsService;
    $this->routeMatch = $routeMatch;
    $this->connection = $connection;
    $this->group = $this->routeMatch->getParameter('group');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('o365.graph'),
      $container->get('o365_groups.groups'),
      $container->get('current_route_match'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'o365_groups_teams';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $groups = $this->getGroups();
    $uuid = FALSE;
    if ($saved = $this->groupsService->getTeamsUuidForGroup($this->group->id())) {
      $uuid = $saved['uuid'];
    }
    $form['uuid'] = [
      '#type' => 'select',
      '#title' => $this->t('Connected Microsoft 365 group'),
      '#options' => $groups,
      '#default_value' => $uuid,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->groupsService->saveTeamsUuidForGroup($form_state->getValue('uuid'), $this->group->id());
  }

  /**
   * Get all the allowed groups from the graph service.
   *
   * @return array
   *   The group options.
   */
  private function getGroups() {
    return array_merge(['' => $this->t('- None -')], $this->groupsService->getGroupsFromTeams());
  }

}
