<?php

/**
 * @file
 * Primary module hooks for Microsoft 365 - Groups module.
 */

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupType;

/**
 * Implements hook_theme().
 */
function o365_groups_theme($existing, $type, $theme, $path) {
  return [
    'o365_groups_file_list_container' => [
      'variables' => [
        'table' => NULL,
      ],
      'render element' => 'children',
    ],
  ];
}

/**
 * Implements hook_entity_extra_field_info().
 */
function o365_groups_entity_extra_field_info() {
  $extra = [];

  foreach (GroupType::loadMultiple() as $bundle) {
    $extra['group'][$bundle->Id()]['display']['o365_group_channel_link'] = [
      'label' => t('Link to Teams channel'),
      'description' => t('This generates a link to the connected Teams channel'),
      'visible' => TRUE,
      'weight' => 100,
    ];
  }

  return $extra;
}

/**
 * Implements hook_ENTITY_TYPE_view().
 */
function o365_groups_group_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  if ($display->getComponent('o365_group_channel_link')) {
    /** @var \Drupal\o365_groups\GroupsService $groupsService */
    $groupsService = \Drupal::service('o365_groups.groups');
    $webUrl = $groupsService->generateTeamChannelLink($entity->id());

    if ($webUrl) {
      $options = [
        'attributes' => [
          'target' => '_blank',
        ],
      ];

      $url = Url::fromUri($webUrl, $options);
      $link = Link::fromTextAndUrl(t('Visit this channel'), $url);
      $build['o365_group_channel_link'] = $link->toRenderable();
    }
  }
}

/**
 * Implements hook_o365_auth_scopes().
 */
function o365_groups_o365_auth_scopes(array &$scopes) {
  $scopes[] = 'Group.Read.All';
  $scopes[] = 'Team.ReadBasic.All';
}
