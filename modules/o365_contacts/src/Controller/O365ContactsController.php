<?php

namespace Drupal\o365_contacts\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\o365\GraphService;
use Drupal\o365_profile\O365ProfileGetDataService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for o365_contacts routes.
 */
final class O365ContactsController extends ControllerBase {

  /**
   * Drupal\o365\GraphService definition.
   *
   * @var \Drupal\o365\GraphService
   */
  protected GraphService $o365Graph;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected ?Request $request;

  /**
   * The profile data service.
   *
   * @var \Drupal\o365_profile\O365ProfileGetDataService
   */
  protected O365ProfileGetDataService $profileInfo;

  /**
   * Constructs a new TeamsGroup object.
   *
   * @param \Drupal\o365\GraphService $o365_graph
   *   The GraphService definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The RequestStack definition.
   * @param \Drupal\o365_profile\O365ProfileGetDataService $profileInfo
   *   The O365ProfileGetDataService definition.
   */
  public function __construct(GraphService $o365_graph, RequestStack $requestStack, O365ProfileGetDataService $profileInfo) {
    $this->o365Graph = $o365_graph;
    $this->request = $requestStack->getCurrentRequest();
    $this->profileInfo = $profileInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('o365.graph'), $container->get('request_stack'), $container->get('o365_profile.get_data'));
  }

  /**
   * Builds the response.
   */
  public function build(): array {
    $form = $this->formBuilder()
      ->getForm('\Drupal\o365_contacts\Form\SearchContactForm');

    $items = [];
    $build = [];

    if (!empty($this->request->get('name'))) {
      $contacts = $this->o365Graph->getGraphData('/me/people/?$search=' . $this->request->get('name'));

      foreach ($contacts['value'] as $contact) {
        if ($contact['userPrincipalName'] !== NULL) {
          $presence = $this->o365Graph->getGraphData('/users/' . $contact['id'] . '/presence')['availability'];

          $items[] = [
            '#theme' => 'o365_contact',
            '#userPrincipalName' => $contact['userPrincipalName'] ?? '',
            '#givenName' => $contact['givenName'] ?? '',
            '#surname' => $contact['surname'] ?? '',
            '#phone_number' => $contact['phones'][0]['number'] ?? '',
            '#presence' => $presence,
            '#email' => $contact['scoredEmailAddresses'][0]['address'],
            '#profile_picture' => $this->profileInfo->getProfileData($contact['id'])['imageSrc'],
            '#teams_call' => 'https://teams.microsoft.com/l/call/0/0?users=' . $contact['userPrincipalName'],
            '#teams_video_call' => 'https://teams.microsoft.com/l/call/0/0?users=' . $contact['userPrincipalName'] . '&Withvideo=true',
            '#teams_chat' => 'https://teams.microsoft.com/l/chat/0/0?users=' . $contact['userPrincipalName'],
          ];
        }
      }
    }

    $build['content'] = [
      '#theme' => 'o365_contacts_search',
      '#form' => $form,
      '#results' => [
        '#theme' => 'item_list',
        '#items' => $items,
      ],
    ];
    return $build;
  }

}
