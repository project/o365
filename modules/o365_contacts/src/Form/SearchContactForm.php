<?php

namespace Drupal\o365_contacts\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a o365_contacts form.
 */
class SearchContactForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'o365_contacts_search_contact';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['contact_search_field'] = [
      '#type' => 'search',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['contact_search_submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $form_state->setRedirect('o365_contacts.search', ['name' => $form_state->getValue('contact_search_field')]);
  }

}
